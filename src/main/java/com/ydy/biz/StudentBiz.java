package com.ydy.biz;

import com.ydy.domain.Student;
import com.ydy.vo.StudentVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StudentBiz {
//    @Transactional(rollbackFor = Exception.class)
//    void insertStudent(Student student);
    void addStudent(Student student);

//    @Transactional(rollbackFor = Exception.class)
//    void addStudent(Student student);

    List<Student> findAll();

    void updateStudentByName(Student student);

    void deleteById(Integer id);

    StudentVo getStudentVo(Integer id);

    void addStudentInfo(StudentVo studentVo);

    StudentVo getStudentVoWithRedis(Integer id);

    StudentVo getStudentVoWithAccount(String s);

    boolean checkAccount(String account, String password);
}
