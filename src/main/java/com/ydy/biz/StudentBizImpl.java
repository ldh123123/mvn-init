package com.ydy.biz;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.ydy.domain.Account;
import com.ydy.domain.Addr;
import com.ydy.domain.Student;
import com.ydy.service.AccountService;
import com.ydy.service.AddrService;
import com.ydy.service.StudentService;
import com.ydy.vo.StudentVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@RequiredArgsConstructor
public class StudentBizImpl implements StudentBiz {

    @Autowired
    private StudentService studentService;
    @Autowired
    private AddrService addrService;
    @Autowired
    private AccountService accountService;

    private final StringRedisTemplate stringRedisTemplate;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addStudent(Student student) {
        studentService.save(student);
    }

    @Override
    public List<Student> findAll() {
        return studentService.list();
    }

    @Override
    public void updateStudentByName(Student student) {
        if (student.getId() == null) {
            throw new RuntimeException("id不能为空");
        }
        Student byId = studentService.getById(student.getId());
        if (byId != null) {
            byId.setName(student.getName());
            studentService.updateById(byId);
        }
    }


    @Override
    public void deleteById(Integer id) {
        studentService.removeById(id);
    }

    @Override
    public StudentVo getStudentVo(Integer id) {
        Student student = studentService.getById(id);
        if (student == null) {
            throw new RuntimeException("用户不存在");
        }
        StudentVo studentVo = new StudentVo();
        studentVo.setId(student.getId());
        studentVo.setName(student.getName());
        Addr addr = addrService.getOne(new LambdaQueryWrapper<Addr>().eq(Addr::getStudentId,id).last("limit 1"));
        if (addr != null) {
            studentVo.setAddr(addr.getAddr());
        }
        Account one = accountService.getOne(new LambdaQueryWrapper<Account>().eq(Account::getStudentId, id).last("limit 1"));
        if (one != null) {
            studentVo.setAccount(one.getAccount());
        }
        return studentVo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addStudentInfo(StudentVo studentVo){
        Student student = new Student();
        studentVo.setName(studentVo.getName());
        studentService.save(student);
        log.info("student id:{}",student.getId());
        Addr addr = new Addr();
        addr.setAddr(studentVo.getAddr());
        addr.setStudentId(student.getId());
        addrService.save(addr);
        Account account = new Account();
        account.setAccount(studentVo.getAccount());
        account.setStudentId(student.getId());
        accountService.save(account);
        log.info("studentVo:{}新增成功",studentVo);
    }
    public static String studentKey="student";
    @Override
    public StudentVo getStudentVoWithRedis(Integer id){
        String s= stringRedisTemplate.opsForValue().get(studentKey +id);
        if (!StringUtils.isEmpty(s)){
            StudentVo studentVo = JSON.parseObject(s,StudentVo.class);
            return studentVo;
        }
        StudentVo studentVo =this.getStudentVo(id);
        stringRedisTemplate.opsForValue().set(studentKey +id,JSON.toJSONString(studentVo),10, TimeUnit.SECONDS);
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return studentVo;
    }
@Override
    public StudentVo getStudentVoWithAccount(String account) {
    StudentVo studentVo = new StudentVo();
    Account one = accountService.getOne(new LambdaQueryWrapper<Account>().eq(Account::getAccount, account).last("limit 1"));
    if (one != null) {
        studentVo.setAccount(one.getAccount());
    }
    Student student = studentService.getById(one.getStudentId());
    studentVo.setId(student.getId());
    studentVo.setName(student.getName());
    studentVo.setPassword(student.getPassword());
    Addr addr = addrService.getOne(new LambdaQueryWrapper<Addr>().eq(Addr::getStudentId, student.getId()).last("limit 1"));

    if (addr != null) {
        studentVo.setAddr(addr.getAddr());
    }
    return studentVo;
}
@Override
    public boolean checkAccount(String account,String password){
        StudentVo studentVoWithAccount = this.getStudentVoWithAccount(account);
        if (null == studentVoWithAccount){
            return false;
        }
        if (studentVoWithAccount.getPassword().equals(password)){
            return true;
        }
        return false;
}
}
