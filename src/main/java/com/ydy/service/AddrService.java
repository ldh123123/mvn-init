package com.ydy.service;

import com.ydy.domain.Addr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 李代花
* @description 针对表【addr】的数据库操作Service
* @createDate 2023-11-15 15:48:55
*/
public interface AddrService extends IService<Addr> {

}
