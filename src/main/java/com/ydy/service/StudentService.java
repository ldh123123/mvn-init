package com.ydy.service;

import com.ydy.domain.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 李代花
* @description 针对表【student】的数据库操作Service
* @createDate 2023-11-09 14:03:28
*/
public interface StudentService extends IService<Student> {

}
