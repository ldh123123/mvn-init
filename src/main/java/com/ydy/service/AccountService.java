package com.ydy.service;

import com.ydy.domain.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 李代花
* @description 针对表【Account】的数据库操作Service
* @createDate 2023-11-15 15:47:17
*/
public interface AccountService extends IService<Account> {

}
