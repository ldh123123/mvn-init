package com.ydy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ydy.domain.Account;
import com.ydy.service.AccountService;
import com.ydy.mapper.AccountMapper;
import org.springframework.stereotype.Service;

/**
* @author 李代花
* @description 针对表【Account】的数据库操作Service实现
* @createDate 2023-11-15 15:47:17
*/
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account>
    implements AccountService{

}




