package com.ydy.service.impl;

import com.github.davidfantasy.jwtshiro.JWTUserAuthService;
import com.github.davidfantasy.jwtshiro.UserInfo;
import com.ydy.biz.StudentBiz;
import com.ydy.vo.StudentVo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
@RequiredArgsConstructor
public class JWTStudentAuthServiceImpl implements JWTUserAuthService {
    @Autowired
    private StudentBiz studentBiz;
    private final StringRedisTemplate stringRedisTemplate;

    @Override
//    public StudentInfo getStudentInfo(String s){
   public UserInfo getUserInfo(String s) {
        StudentVo studentVoWithRedis = studentBiz.getStudentVoWithAccount(s);
//        StudentInfo studentInfo = new StudentInfo();
        UserInfo userInfo = new UserInfo();
        if (null == studentVoWithRedis.getId()) {
            return null;
        }
        userInfo.setAccount(studentVoWithRedis.getAccount());
        userInfo.setSecret(studentVoWithRedis.getPassword());
        return userInfo;
    }
//        if (null == studentVoWithRedis.getId()) {
//            return null;
//        }
//        studentInfo.setAccount(studentVoWithRedis.getAccount());
//        studentInfo.setSecret(studentVoWithRedis.getPassword());
//        return studentInfo;
//    }




    @Override
    public void onAuthenticationFailed(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
    }

    @Override
    public void onAuthorizationFailed(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
    }
}
