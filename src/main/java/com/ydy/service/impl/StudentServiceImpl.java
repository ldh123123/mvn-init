package com.ydy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ydy.domain.Student;
import com.ydy.service.StudentService;
import com.ydy.mapper.StudentMapper;
import org.springframework.stereotype.Service;

/**
* @author 李代花
* @description 针对表【student】的数据库操作Service实现
* @createDate 2023-11-09 14:03:28
*/
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student>
    implements StudentService{

}




