package com.ydy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ydy.domain.Addr;
import com.ydy.service.AddrService;
import com.ydy.mapper.AddrMapper;
import org.springframework.stereotype.Service;

/**
* @author 李代花
* @description 针对表【addr】的数据库操作Service实现
* @createDate 2023-11-15 15:48:55
*/
@Service
public class AddrServiceImpl extends ServiceImpl<AddrMapper, Addr>
    implements AddrService{

}




