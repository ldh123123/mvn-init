package com.ydy.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.davidfantasy.jwtshiro.JWTHelper;
import com.github.davidfantasy.jwtshiro.annotation.AlowAnonymous;
import com.ydy.biz.StudentBiz;
import com.ydy.domain.Student;
import com.ydy.service.StudentService;
import com.ydy.vo.JsonResult;
import com.ydy.vo.StudentVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class HelloController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentBiz studentBiz;

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @GetMapping("/student")

    public List<Student> hello() {
        return studentBiz.findAll();
    }

    /**
     * connect
     * <p>
     * select * from user where id = ?
     * Student student = new Student ();
     * user.setId()
     * user.setName{}
     *
     * @param id
     * @return
     */
    @GetMapping("/getById")
    public Student getStudent(Integer id) {
        return studentService.getById(id);
    }

    /**
     * select * from user where name = ?
     *
     * @param name
     * @return
     */
    @GetMapping("/getByName")
    public Student getStudent(String name) {
        LambdaQueryWrapper<Student> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(Student::getName, name);
        List<Student> list = studentService.list(wrapper);
        if (list.isEmpty()) {
            return null;
        }
//        userService.getOne(wrapper.last("limit 1"));
        return list.get(0);
    }

    @PostMapping("/insert")
    public void insert(@RequestBody Student student) {
        if (StringUtils.isEmpty(student.getName())) {
            return;
        }
        studentBiz.addStudent(student);
    }


    @PostMapping("/update")
    public void updateStudentByName(@RequestBody Student student) {
        studentBiz.updateStudentByName(student);
    }

    @DeleteMapping("/deleteById")
    public void deleteById(Integer id) {
        studentBiz.deleteById(id);
    }
    @GetMapping("/getStudentVo")
    public StudentVo getStudentVo(Integer id){
        return studentBiz.getStudentVo(id);
    }

@PostMapping("/addStudent")
    public void addStudent(@RequestBody StudentVo studentVo){
        studentBiz.addStudentInfo(studentVo);
    }
   @GetMapping("/getStudentVoWithRedis")
   public StudentVo getStudentVoWithRedis(Integer id){
        return studentBiz.getStudentVoWithRedis(id);
   }
   @Autowired
    private JWTHelper jwtHelper;
    @AlowAnonymous
    @GetMapping("/login")
    public JsonResult login(String account, String password){
        boolean b= studentBiz.checkAccount(account,password);
        if (!b){
            return JsonResult.fail("账号或密码错误");

        }
        String token = jwtHelper.sign(account,password);
        return JsonResult.suc(token);
    }
}