package com.ydy.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/redis")
public class RedisController {
    private final RedisTemplate<String,Object> redisTemplate;
    @GetMapping("/get")
    public String get(String key){
        return (String) redisTemplate.opsForValue().get(key);
    }
    @GetMapping("/set")
    public void set(String key,String value){
        redisTemplate.opsForValue().set(key,value);
        String o =(String)redisTemplate.opsForValue().get(key);
        log.info("set key:{} value:{}",key,o);
    }
}
