package com.ydy.vo;

import lombok.Data;

@Data
public class JsonResult {
    private String code;
    private String msg;
    private Object date;
    public JsonResult(String number,String msg,Object o){
        this.code=number;
        this.msg=msg;
        this.date=o;
    }
    public static JsonResult fail(String msg){
        return new JsonResult("500",msg,null);
    }
    public static JsonResult suc(Object o){
        return new JsonResult("200",null,o);
    }
}
