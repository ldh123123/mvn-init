package com.ydy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * @TableName Account
 */
@TableName(value ="Account")
@Data
public class Account implements Serializable {
    private Integer id;

    private String name;

    private Integer studentId;

    private String account;

    private static final long serialVersionUID = 1L;
}