package com.ydy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * @TableName student
 */
@TableName(value ="student")
@Data
public class Student implements Serializable {
    public String Password;
    private Integer id;

    private String name;

    private Integer age;

    private String sex;

    private String address;

    private Integer math;

    private Integer english;

    private static final long serialVersionUID = 1L;
}