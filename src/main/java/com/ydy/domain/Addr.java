package com.ydy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * @TableName addr
 */
@TableName(value ="addr")
@Data
public class Addr implements Serializable {
    private Integer cid;

    private String cname;

    private Integer studentId;

    private String addr;

    private static final long serialVersionUID = 1L;
}