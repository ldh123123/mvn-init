package com.ydy.mapper;

import com.ydy.domain.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 李代花
* @description 针对表【student】的数据库操作Mapper
* @createDate 2023-11-09 14:03:28
* @Entity com.ydy.domain.Student
*/
@Mapper
public interface StudentMapper extends BaseMapper<Student> {

}




