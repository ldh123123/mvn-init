package com.ydy.mapper;

import com.ydy.domain.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 李代花
* @description 针对表【Account】的数据库操作Mapper
* @createDate 2023-11-15 15:47:17
* @Entity com.ydy.domain.Account
*/
public interface AccountMapper extends BaseMapper<Account> {

}




