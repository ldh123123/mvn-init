package com.ydy.mapper;

import com.ydy.domain.Addr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 李代花
* @description 针对表【addr】的数据库操作Mapper
* @createDate 2023-11-15 15:48:55
* @Entity com.ydy.domain.Addr
*/
public interface AddrMapper extends BaseMapper<Addr> {

}




